# typejson #

[![Build Status][travis-badge]][travis-badge-url]
[![Codacy Badge][codacy-badge]][codacy-badge-url]
[![Codacy Badge][coverage-badge]][coverage-badge-url]
[![Dependency Status][gemnasium-badge]][gemnasium-badge-url]
[![npm][npm-badge]][npm-badge-url]

**A serialization library for typescipt**

### Installation ###

The latest release of typejson can be installed from npm
```
    npm install typejson --save
```

Reflect-Metadata features are used, so you may want to install reflect-metadata too:
```
    npm install reflect-metadata --save
```

### Basic Usage ###

TODO 

### License ###

MIT 

[travis-badge]: https://travis-ci.org/ttgillieron/typejson.svg?branch=master
[travis-badge-url]: https://travis-ci.org/ttgillieron/typejson

[codacy-badge]: https://api.codacy.com/project/badge/Grade/9600a5dff8c74b16b030f40eab973de7
[codacy-badge-url]: https://www.codacy.com/app/thomasgillieron/typejson?utm_source=ttgillieron@bitbucket.org&amp;utm_medium=referral&amp;utm_content=ttgillieron/typejson&amp;utm_campaign=Badge_Grade

[coverage-badge]: https://api.codacy.com/project/badge/Coverage/9600a5dff8c74b16b030f40eab973de7
[coverage-badge-url]: https://www.codacy.com/app/thomasgillieron/typejson?utm_source=ttgillieron@bitbucket.org&utm_medium=referral&utm_content=ttgillieron/typejson&utm_campaign=Badge_Coverage

[gemnasium-badge]: https://gemnasium.com/badges/bitbucket.org/ttgillieron/typejson.svg
[gemnasium-badge-url]: https://gemnasium.com/bitbucket.org/ttgillieron/typejson

[npm-badge]: https://img.shields.io/npm/v/typejson.svg
[npm-badge-url]: https://www.npmjs.com/package/typejson