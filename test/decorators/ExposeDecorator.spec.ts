import {KEY_EXPOSE} from '../../src/definitions/PropertyDefinition';
import {Expose} from '../../src/decorators/ExposeDecorator';
import {nofct, ObjectStrategy} from '../../src/core/Context';

function Dumy(target: any, property?: string) {
}

export class TestDefinitionWithDecorator {
  @Dumy property: string;
}

describe('Expose decorator ', () => {

  describe('allow to add definition', () => {

    it('without metadata', () => {
      const instance = new TestDefinitionWithDecorator();
      Expose()(instance, 'property');

      const def = Reflect.getMetadata(KEY_EXPOSE, instance, 'property')[0];

      expect(def.property).toEqual('property');
      expect(def.plain).toEqual('property');
      expect(def.type).toEqual(String);
      expect(def.generic).toEqual(String);
      expect(def.value).toEqual(null);
      expect(def.groups).toEqual([]);
      expect(def.excludeToPlain).toEqual(false);
      expect(def.excludeFromPlain).toEqual(false);
      expect(def.toPlain).toEqual(nofct);
      expect(def.fromPlain).toEqual(nofct);
      expect(def.strategy).toEqual(ObjectStrategy.NONE);
    });

    it('with metadata plain', () => {
      const instance = new TestDefinitionWithDecorator();
      Expose({plain: 'plain'})(instance, 'property');

      const def = Reflect.getMetadata(KEY_EXPOSE, instance, 'property')[0];

      expect(def.property).toEqual('property');
      expect(def.plain).toEqual('plain');
      expect(def.type).toEqual(String);
      expect(def.generic).toEqual(String);
      expect(def.value).toEqual(null);
      expect(def.groups).toEqual([]);
      expect(def.excludeToPlain).toEqual(false);
      expect(def.excludeFromPlain).toEqual(false);
      expect(def.toPlain).toEqual(nofct);
      expect(def.fromPlain).toEqual(nofct);
      expect(def.strategy).toEqual(ObjectStrategy.NONE);
    });

    it('with metadata type', () => {
      const instance = new TestDefinitionWithDecorator();
      Expose({type: Array})(instance, 'property');

      const def = Reflect.getMetadata(KEY_EXPOSE, instance, 'property')[0];

      expect(def.property).toEqual('property');
      expect(def.plain).toEqual('property');
      expect(def.type).toEqual(Array);
      expect(def.generic).toEqual(String);
      expect(def.value).toEqual(null);
      expect(def.groups).toEqual([]);
      expect(def.excludeToPlain).toEqual(false);
      expect(def.excludeFromPlain).toEqual(false);
      expect(def.toPlain).toEqual(nofct);
      expect(def.fromPlain).toEqual(nofct);
      expect(def.strategy).toEqual(ObjectStrategy.NONE);
    });

    it('with metadata value', () => {
      const instance = new TestDefinitionWithDecorator();
      Expose({value: 'value'})(instance, 'property');

      const def = Reflect.getMetadata(KEY_EXPOSE, instance, 'property')[0];

      expect(def.property).toEqual('property');
      expect(def.plain).toEqual('property');
      expect(def.type).toEqual(String);
      expect(def.generic).toEqual(String);
      expect(def.value).toEqual('value');
      expect(def.groups).toEqual([]);
      expect(def.excludeToPlain).toEqual(false);
      expect(def.excludeFromPlain).toEqual(false);
      expect(def.toPlain).toEqual(nofct);
      expect(def.fromPlain).toEqual(nofct);
      expect(def.strategy).toEqual(ObjectStrategy.NONE);
    });

    it('with metadata groups', () => {
      const instance = new TestDefinitionWithDecorator();
      Expose({groups: ['groups']})(instance, 'property');

      const def = Reflect.getMetadata(KEY_EXPOSE, instance, 'property')[0];

      expect(def.property).toEqual('property');
      expect(def.plain).toEqual('property');
      expect(def.type).toEqual(String);
      expect(def.generic).toEqual(String);
      expect(def.value).toEqual(null);
      expect(def.groups).toEqual(['groups']);
      expect(def.excludeToPlain).toEqual(false);
      expect(def.excludeFromPlain).toEqual(false);
      expect(def.toPlain).toEqual(nofct);
      expect(def.fromPlain).toEqual(nofct);
      expect(def.strategy).toEqual(ObjectStrategy.NONE);
    });

    it('with metadata excludeToPlain', () => {
      const instance = new TestDefinitionWithDecorator();
      Expose({excludeToPlain: true})(instance, 'property');

      const def = Reflect.getMetadata(KEY_EXPOSE, instance, 'property')[0];

      expect(def.property).toEqual('property');
      expect(def.plain).toEqual('property');
      expect(def.type).toEqual(String);
      expect(def.generic).toEqual(String);
      expect(def.value).toEqual(null);
      expect(def.groups).toEqual([]);
      expect(def.excludeToPlain).toEqual(true);
      expect(def.excludeFromPlain).toEqual(false);
      expect(def.toPlain).toEqual(nofct);
      expect(def.fromPlain).toEqual(nofct);
      expect(def.strategy).toEqual(ObjectStrategy.NONE);
    });

    it('with metadata excludeFromPlain', () => {
      const instance = new TestDefinitionWithDecorator();
      Expose({excludeFromPlain: true})(instance, 'property');

      const def = Reflect.getMetadata(KEY_EXPOSE, instance, 'property')[0];

      expect(def.property).toEqual('property');
      expect(def.plain).toEqual('property');
      expect(def.type).toEqual(String);
      expect(def.generic).toEqual(String);
      expect(def.value).toEqual(null);
      expect(def.groups).toEqual([]);
      expect(def.excludeToPlain).toEqual(false);
      expect(def.excludeFromPlain).toEqual(true);
      expect(def.toPlain).toEqual(nofct);
      expect(def.fromPlain).toEqual(nofct);
      expect(def.strategy).toEqual(ObjectStrategy.NONE);
    });

    it('with metadata toPlain', () => {
      const toPlain = (): any => null;
      const instance = new TestDefinitionWithDecorator();
      Expose({toPlain: toPlain})(instance, 'property');

      const def = Reflect.getMetadata(KEY_EXPOSE, instance, 'property')[0];

      expect(def.property).toEqual('property');
      expect(def.plain).toEqual('property');
      expect(def.type).toEqual(String);
      expect(def.generic).toEqual(String);
      expect(def.value).toEqual(null);
      expect(def.groups).toEqual([]);
      expect(def.excludeToPlain).toEqual(false);
      expect(def.excludeFromPlain).toEqual(false);
      expect(def.toPlain).toEqual(toPlain);
      expect(def.fromPlain).toEqual(nofct);
      expect(def.strategy).toEqual(ObjectStrategy.NONE);
    });

    it('with metadata fromPlain', () => {
      const fromPlain = (): any => null;
      const instance = new TestDefinitionWithDecorator();
      Expose({fromPlain: fromPlain})(instance, 'property');

      const def = Reflect.getMetadata(KEY_EXPOSE, instance, 'property')[0];

      expect(def.property).toEqual('property');
      expect(def.plain).toEqual('property');
      expect(def.type).toEqual(String);
      expect(def.generic).toEqual(String);
      expect(def.value).toEqual(null);
      expect(def.groups).toEqual([]);
      expect(def.excludeToPlain).toEqual(false);
      expect(def.excludeFromPlain).toEqual(false);
      expect(def.toPlain).toEqual(nofct);
      expect(def.fromPlain).toEqual(fromPlain);
      expect(def.strategy).toEqual(ObjectStrategy.NONE);
    });

    it('with metadata strategy', () => {
      const instance = new TestDefinitionWithDecorator();
      Expose({strategy: ObjectStrategy.FLAT})(instance, 'property');

      const def = Reflect.getMetadata(KEY_EXPOSE, instance, 'property')[0];

      expect(def.property).toEqual('property');
      expect(def.plain).toEqual('property');
      expect(def.type).toEqual(String);
      expect(def.generic).toEqual(String);
      expect(def.value).toEqual(null);
      expect(def.groups).toEqual([]);
      expect(def.excludeToPlain).toEqual(false);
      expect(def.excludeFromPlain).toEqual(false);
      expect(def.toPlain).toEqual(nofct);
      expect(def.fromPlain).toEqual(nofct);
      expect(def.strategy).toEqual(ObjectStrategy.FLAT);
    });
  });
});
