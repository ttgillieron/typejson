import {RegexpConverter} from '../../src/converters/RegexpConverter';
describe('RegexpConverter', () => {

  const converter = new RegexpConverter();

  it('match', () => {
    expect(converter.match(RegExp)).toEqual(true);
    expect(converter.match(null)).toEqual(false);
  });

  it('toPlainObject', () => {
    expect(converter.toPlainObject(new RegExp('test'))).toEqual('/test/');
  });

  it('fromPlainObject', () => {
    expect(converter.fromPlainObject(RegExp, 'test')).toEqual(/test/);
  });
});
