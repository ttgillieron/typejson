import {ObjectConverter} from '../../src/converters/ObjectConverter';
import {Context, ObjectStrategy} from '../../src/core/Context';
import {CONVERTERS} from '../../src/converters/index';
import {
  TestDefinition,
  TestDefinitionCyclic,
  TestDefinitionExclude,
  TestDefinitionFunction,
  TestDefinitionGroup,
  TestDefinitionHook,
  TestDefinitionName,
  TestDefinitionStrategy,
  TestDefinitionStrategyContext,
  TestDefinitionStrategyFlat,
  TestDefinitionStrategyNone,
  TestDefinitionStrategyTree,
  TestDefinitionValue
} from './utils-object-converter';

describe('ObjectConverter', () => {

  const converter = new ObjectConverter();

  it('match', () => {
    expect(converter.match(Object)).toEqual(true);
    expect(converter.match(null)).toEqual(false);
  });

  it('toPlainObject', () => {
    expect(converter.toPlainObject({}, new Context(CONVERTERS, CONVERTERS[8]))).toEqual({});
  });

  it('fromPlainObject', () => {
    expect(converter.fromPlainObject(Object, {}, new Context(CONVERTERS, CONVERTERS[8]))).toEqual({});
  });

  describe('can play hook', () => {
    it('onToPlainObject', () => {
      expect(converter.toPlainObject(new TestDefinitionHook(), new Context(CONVERTERS, CONVERTERS[8]))).toEqual({});
    });

    it('onFromPlainObject', () => {
      expect(converter.fromPlainObject(TestDefinitionHook, {}, new Context(CONVERTERS, CONVERTERS[8]))).toEqual(new TestDefinitionHook());
    });
  });

  describe('can converte property', () => {
    it('onToPlainObject', () => {
      expect(converter.toPlainObject(new TestDefinition('toto'), new Context(CONVERTERS, CONVERTERS[8]))).toEqual({property: 'toto'});
    });

    it('onFromPlainObject', () => {
      expect(converter.fromPlainObject(TestDefinition, {property: 'toto'}, new Context(CONVERTERS, CONVERTERS[8])))
        .toEqual(new TestDefinition('toto'));
    });
  });

  describe('can use custom function', () => {
    it('onToPlainObject', () => {
      expect(converter.toPlainObject(new TestDefinitionFunction(), new Context(CONVERTERS, CONVERTERS[8]))).toEqual({property: 'toPlain'});
    });

    it('onFromPlainObject', () => {
      expect(converter.fromPlainObject(TestDefinitionFunction, {property: 'toto'}, new Context(CONVERTERS, CONVERTERS[8])))
        .toEqual(new TestDefinitionFunction('fromPlain'));
    });
  });

  describe('can use custom name', () => {
    it('onToPlainObject', () => {
      expect(converter.toPlainObject(new TestDefinitionName('toto'), new Context(CONVERTERS, CONVERTERS[8]))).toEqual({custom: 'toto'});
    });

    it('onFromPlainObject', () => {
      expect(converter.fromPlainObject(TestDefinitionName, {custom: 'toto'}, new Context(CONVERTERS, CONVERTERS[8])))
        .toEqual(new TestDefinitionName('toto'));
    });
  });

  describe('can use default value', () => {
    it('onToPlainObject', () => {
      expect(converter.toPlainObject(new TestDefinitionValue(), new Context(CONVERTERS, CONVERTERS[8]))).toEqual({property: 'toto'});
    });
  });

  describe('can exclude property', () => {

    describe('with definition property', () => {
      it('onToPlainObject', () => {
        expect(converter.toPlainObject(new TestDefinitionExclude('toto', 'toto'), new Context(CONVERTERS, CONVERTERS[8])))
          .toEqual({property1: 'toto'});
      });

      it('onFromPlainObject', () => {
        expect(converter.fromPlainObject(TestDefinitionExclude, {
          property1: 'toto',
          property2: 'toto'
        }, new Context(CONVERTERS, CONVERTERS[8])))
          .toEqual(new TestDefinitionExclude(void 0, 'toto'));
      });
    });

    describe('with definition group', () => {
      it('onToPlainObject', () => {
        expect(converter.toPlainObject(new TestDefinitionGroup('toto', 'toto'), new Context(CONVERTERS, CONVERTERS[8], ['tata'])))
          .toEqual({property1: 'toto'});
      });

      it('onFromPlainObject', () => {
        expect(converter.fromPlainObject(TestDefinitionGroup, {
          property1: 'toto',
          property2: 'toto'
        }, new Context(CONVERTERS, CONVERTERS[8], ['titi'])))
          .toEqual(new TestDefinitionGroup(void 0, 'toto'));
      });
    });

    describe('with cyclic reference', () => {
      it('onToPlainObject', () => {
        const test1 = new TestDefinitionCyclic('toto');
        test1.property2 = new TestDefinitionCyclic('titi', test1);

        expect(converter.toPlainObject(test1, new Context(CONVERTERS, CONVERTERS[8])))
          .toEqual({property1: 'toto', property2: {property1: 'titi'}});
      });
    });
  });

  describe('can use strategy', () => {
    describe('from context', () => {
      describe('none', () => {
        it('onToPlainObject', () => {
          expect(converter.toPlainObject(
            new TestDefinitionStrategyContext('toto', new TestDefinitionStrategy('toto')),
            new Context(CONVERTERS, CONVERTERS[8], [], ObjectStrategy.NONE))
          ).toEqual({property1: 'toto', property2: {property3: 'toto'}});
        });

        it('onFromPlainObject', () => {
          expect(converter.fromPlainObject(
            TestDefinitionStrategyContext,
            {property1: 'toto', property2: {property3: 'toto'}},
            new Context(CONVERTERS, CONVERTERS[8], [], ObjectStrategy.NONE))
          ).toEqual(new TestDefinitionStrategyContext('toto', new TestDefinitionStrategy('toto')));
        });
      });

      describe('tree', () => {
        it('onToPlainObject', () => {
          expect(converter.toPlainObject(
            new TestDefinitionStrategyContext('toto', new TestDefinitionStrategy('toto')),
            new Context(CONVERTERS, CONVERTERS[8], [], ObjectStrategy.TREE))
          ).toEqual({property1: 'toto', property2: {property3: 'toto'}});
        });

        it('onFromPlainObject', () => {
          expect(converter.fromPlainObject(
            TestDefinitionStrategyContext,
            {property1: 'toto', property2: {property3: 'toto'}},
            new Context(CONVERTERS, CONVERTERS[8], [], ObjectStrategy.TREE))
          ).toEqual(new TestDefinitionStrategyContext('toto', new TestDefinitionStrategy('toto')));
        });
      });

      describe('flat', () => {
        it('onToPlainObject', () => {
          expect(converter.toPlainObject(
            new TestDefinitionStrategyContext('toto', new TestDefinitionStrategy('toto')),
            new Context(CONVERTERS, CONVERTERS[8], [], ObjectStrategy.FLAT))
          ).toEqual({property1: 'toto', property3: 'toto'});
        });

        it('onFromPlainObject', () => {
          expect(converter.fromPlainObject(
            TestDefinitionStrategyContext,
            {property1: 'toto', property3: 'toto'},
            new Context(CONVERTERS, CONVERTERS[8], [], ObjectStrategy.FLAT))
          ).toEqual(new TestDefinitionStrategyContext('toto', new TestDefinitionStrategy('toto')));
        });
      });
    });

    describe('from definition', () => {
      describe('none', () => {
        it('onToPlainObject', () => {
          expect(converter.toPlainObject(
            new TestDefinitionStrategyNone('toto', new TestDefinitionStrategy('toto')),
            new Context(CONVERTERS, CONVERTERS[8], []))
          ).toEqual({property1: 'toto', property2: {property3: 'toto'}});
        });

        it('onFromPlainObject', () => {
          expect(converter.fromPlainObject(
            TestDefinitionStrategyNone,
            {property1: 'toto', property2: {property3: 'toto'}},
            new Context(CONVERTERS, CONVERTERS[8], []))
          ).toEqual(new TestDefinitionStrategyNone('toto', new TestDefinitionStrategy('toto')));
        });
      });

      describe('tree', () => {
        it('onToPlainObject', () => {
          expect(converter.toPlainObject(
            new TestDefinitionStrategyTree('toto', new TestDefinitionStrategy('toto')),
            new Context(CONVERTERS, CONVERTERS[8], []))
          ).toEqual({property1: 'toto', property2: {property3: 'toto'}});
        });

        it('onFromPlainObject', () => {
          expect(converter.fromPlainObject(
            TestDefinitionStrategyTree,
            {property1: 'toto', property2: {property3: 'toto'}},
            new Context(CONVERTERS, CONVERTERS[8], []))
          ).toEqual(new TestDefinitionStrategyTree('toto', new TestDefinitionStrategy('toto')));
        });
      });

      describe('flat', () => {
        it('onToPlainObject', () => {
          expect(converter.toPlainObject(
            new TestDefinitionStrategyFlat('toto', new TestDefinitionStrategy('toto')),
            new Context(CONVERTERS, CONVERTERS[8], []))
          ).toEqual({property1: 'toto', property3: 'toto'});
        });

        it('onFromPlainObject', () => {
          expect(converter.fromPlainObject(
            TestDefinitionStrategyFlat,
            {property1: 'toto', property3: 'toto'},
            new Context(CONVERTERS, CONVERTERS[8], []))
          ).toEqual(new TestDefinitionStrategyFlat('toto', new TestDefinitionStrategy('toto'), new TestDefinitionStrategy('toto')));
        });
      });
    });
  });
});
