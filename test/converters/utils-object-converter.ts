import {Expose} from '../../src/decorators/ExposeDecorator';
import {Context, ObjectStrategy} from '../../src/core/Context';

export class TestDefinitionHook {
  property: string;

  onToPlainObject(instance: any, plain: any, context: Context): void {}

  onFromPlainObject(instance: any, plain: any, context: Context): void {}
}

export class TestDefinition {
  @Expose() property: string;

  constructor(property?: string) {
    this.property = property;
  }
}

export class TestDefinitionFunction {
  @Expose({toPlain: () => 'toPlain', fromPlain: () => 'fromPlain'}) property: string;

  constructor(property?: string) {
    this.property = property;
  }
}

export class TestDefinitionName {
  @Expose({plain: 'custom'}) property: string;

  constructor(property?: string) {
    this.property = property;
  }
}

export class TestDefinitionValue {
  @Expose({value: 'toto'}) property: string;

  constructor(property?: string) {
    this.property = property;
  }
}

export class TestDefinitionExclude {
  @Expose({excludeFromPlain: true}) property1: string;
  @Expose({excludeToPlain: true}) property2: string;

  constructor(property1?: string, property2?: string) {
    this.property1 = property1;
    this.property2 = property2;
  }
}

export class TestDefinitionGroup {
  @Expose({groups: ['tata']}) property1: string;
  @Expose({groups: ['titi']}) property2: string;

  constructor(property1?: string, property2?: string) {
    this.property1 = property1;
    this.property2 = property2;
  }
}

export class TestDefinitionCyclic {
  @Expose() property1: string;
  @Expose() property2: TestDefinitionCyclic;

  constructor(property1?: string, property2?: TestDefinitionCyclic) {
    this.property1 = property1;
    this.property2 = property2;
  }
}

export class TestDefinitionStrategy {
  @Expose() property3: string;

  constructor(property3?: string) {
    this.property3 = property3;
  }
}

export class TestDefinitionStrategyContext {
  @Expose() property1: string;
  @Expose() property2: TestDefinitionStrategy;

  constructor(property1?: string, property2?: TestDefinitionStrategy) {
    this.property1 = property1;
    this.property2 = property2;
  }
}

export class TestDefinitionStrategyNone {
  @Expose({strategy: ObjectStrategy.NONE}) property1: string;
  @Expose({strategy: ObjectStrategy.NONE}) property2: TestDefinitionStrategy;

  constructor(property1?: string, property2?: TestDefinitionStrategy) {
    this.property1 = property1;
    this.property2 = property2;
  }
}

export class TestDefinitionStrategyTree {
  @Expose({strategy: ObjectStrategy.TREE}) property1: string;
  @Expose({strategy: ObjectStrategy.TREE}) property2: TestDefinitionStrategy;

  constructor(property1?: string, property2?: TestDefinitionStrategy) {
    this.property1 = property1;
    this.property2 = property2;
  }
}

export class TestDefinitionStrategyFlat {
  @Expose({strategy: ObjectStrategy.FLAT}) property1: string;
  @Expose({strategy: ObjectStrategy.FLAT, excludeToPlain: true, fromPlain: () => new TestDefinitionStrategy('toto')}) property4: TestDefinitionStrategy;
  @Expose({strategy: ObjectStrategy.FLAT}) property2: TestDefinitionStrategy;

  constructor(property1?: string, property2?: TestDefinitionStrategy, property4?: TestDefinitionStrategy) {
    this.property1 = property1;
    this.property2 = property2;
    this.property4 = property4;
  }
}
