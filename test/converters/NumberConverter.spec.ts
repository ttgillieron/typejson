import {NumberConverter} from '../../src/converters/NumberConverter';
describe('NumberConverter', () => {

  const converter = new NumberConverter();

  it('match', () => {
    expect(converter.match(Number)).toEqual(true);
    expect(converter.match(null)).toEqual(false);
  });

  it('toPlainObject', () => {
    expect(converter.toPlainObject(1)).toEqual(1);
  });

  it('fromPlainObject', () => {
    expect(converter.fromPlainObject(Number, 1)).toEqual(1);
  });
});
