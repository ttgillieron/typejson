import {DateConverter} from '../../src/converters/DateConverter';
describe('DateConverter', () => {

  const converter = new DateConverter();

  it('match', () => {
    expect(converter.match(Date)).toEqual(true);
    expect(converter.match(null)).toEqual(false);
  });

  it('toPlainObject', () => {
    expect(converter.toPlainObject(new Date('2017-06-05T16:12:45.049Z'))).toEqual('2017-06-05T16:12:45.049Z');
  });

  it('fromPlainObject', () => {
    expect(converter.fromPlainObject(Date, '2017-06-05T16:12:45.049Z')).toEqual(new Date('2017-06-05T16:12:45.049Z'));
  });
});
