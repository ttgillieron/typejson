import {BooleanConverter} from '../../src/converters/BooleanConverter';
describe('BooleanConverter', () => {

  const converter = new BooleanConverter();

  it('match', () => {
    expect(converter.match(Boolean)).toEqual(true);
    expect(converter.match(null)).toEqual(false);
  });

  it('toPlainObject', () => {
    expect(converter.toPlainObject(true)).toEqual(true);
  });

  it('fromPlainObject', () => {
    expect(converter.fromPlainObject(Boolean, true)).toEqual(true);
  });
});
