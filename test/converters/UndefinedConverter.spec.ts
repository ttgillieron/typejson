import {UndefinedConverter} from '../../src/converters/UndefinedConverter';
describe('UndefinedConverter', () => {

  const converter = new UndefinedConverter();

  it('match', () => {
    expect(converter.match(void 0)).toEqual(true);
    expect(converter.match(null)).toEqual(false);
  });

  it('toPlainObject', () => {
    expect(converter.toPlainObject()).toEqual(void 0);
  });

  it('fromPlainObject', () => {
    expect(converter.fromPlainObject()).toEqual(void 0);
  });
});
