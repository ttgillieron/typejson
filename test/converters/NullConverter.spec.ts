import {NullConverter} from '../../src/converters/NullConverter';

describe('NullConverter', () => {

  const converter = new NullConverter();

  it('match', () => {
    expect(converter.match(null)).toEqual(true);
    expect(converter.match(void 0)).toEqual(false);
  });

  it('toPlainObject', () => {
    expect(converter.toPlainObject()).toEqual(null);
  });

  it('fromPlainObject', () => {
    expect(converter.fromPlainObject()).toEqual(null);
  });
});
