import {ArrayConverter} from '../../src/converters/ArrayConverter';
import {CONVERTERS} from '../../src/converters/index';
import {Context} from '../../src/core/Context';
describe('StringConverter', () => {

  const converter = new ArrayConverter();

  it('match', () => {
    expect(converter.match(Array)).toEqual(true);
    expect(converter.match(null)).toEqual(false);
  });

  it('toPlainObject', () => {
    expect(converter.toPlainObject(['test'], new Context(CONVERTERS, CONVERTERS[8]))).toEqual(['test']);
  });

  it('fromPlainObject', () => {
    expect(converter.fromPlainObject(String, ['test'], new Context(CONVERTERS, CONVERTERS[8]))).toEqual(['test']);
  });
});
