import {StringConverter} from '../../src/converters/StringConverter';
describe('StringConverter', () => {

  const converter = new StringConverter();

  it('match', () => {
    expect(converter.match(String)).toEqual(true);
    expect(converter.match(null)).toEqual(false);
  });

  it('toPlainObject', () => {
    expect(converter.toPlainObject('test')).toEqual('test');
  });

  it('fromPlainObject', () => {
    expect(converter.fromPlainObject(String, 'test')).toEqual('test');
  });
});
