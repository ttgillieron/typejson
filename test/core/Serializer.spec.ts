import {Serializer} from '../../src/core/Serializer';
import {CONVERTERS} from '../../src/converters/index';
import {ObjectConverter} from '../../src/converters/ObjectConverter';
import {Context} from '../../src/core/Context';

export class TestDefinition {
  property: string;
}

describe('Serializer ', () => {

  describe('can be instanced from constructor', () => {

    it('with no default', () => {
      const defaultConverter = new ObjectConverter();
      const serializer = new Serializer();

      expect(serializer['context'].converters).toEqual(CONVERTERS);
      expect(serializer['context'].defaultConverter).toEqual(defaultConverter);
      expect(serializer['context'].groups).toEqual([]);
      expect(serializer['context'].cyclic).toEqual([]);
    });

    it('with context', () => {
      const defaultConverter = new ObjectConverter();
      const context = new Context(CONVERTERS, defaultConverter, ['test'], [{}]);
      const serializer = new Serializer(context);

      expect(serializer['context']).toEqual(context);
    });
  });

  describe('can convert json', () => {
    it('toJson without option', () => {
      const serializer = new Serializer();
      expect(serializer.toJson(new TestDefinition())).toEqual('{}');
    });

    it('toJson with option', () => {
      const serializer = new Serializer();
      expect(serializer.toJson(new TestDefinition(), {})).toEqual('{}');
    });

    it('fromJson without option', () => {
      const serializer = new Serializer();
      expect(serializer.fromJson(TestDefinition, '{}')).toEqual(new TestDefinition());
    });

    it('fromJson with option', () => {
      const serializer = new Serializer();
      expect(serializer.fromJson(TestDefinition, '{}', {})).toEqual(new TestDefinition());
    });
  });

  describe('can convert plain object', () => {
    it('toPlainObject without option', () => {
      const serializer = new Serializer();
      expect(serializer.toPlainObject(new TestDefinition())).toEqual({});
    });

    it('toPlainObject with option', () => {
      const serializer = new Serializer();
      expect(serializer.toPlainObject(new TestDefinition(), {})).toEqual({});
    });

    it('toPlainObject with array', () => {
      const serializer = new Serializer();
      expect(serializer.toPlainObject([new TestDefinition()], {})).toEqual([{}]);
    });

    it('fromPlainObject without option', () => {
      const serializer = new Serializer();
      expect(serializer.fromPlainObject(TestDefinition, {})).toEqual(new TestDefinition());
    });

    it('fromPlainObject with option', () => {
      const serializer = new Serializer();
      expect(serializer.fromPlainObject(TestDefinition, {}, {})).toEqual(new TestDefinition());
    });

    it('fromPlainObject with array', () => {
      const serializer = new Serializer();
      expect(serializer.fromPlainObject(TestDefinition, [{}], {})).toEqual([new TestDefinition()]);
    });
  });
});
