import {Context, CONVERTERS} from '../../src';
import {ObjectConverter} from '../../src/converters/ObjectConverter';
import {nofct, ObjectStrategy} from '../../src/core/Context';

describe('Context ', () => {

  // nofct call for coverage - no test needed
  nofct();

  describe('can be instanced from constructor', () => {

    it('with no default', () => {
      const defaultConverter = new ObjectConverter();
      const context = new Context(CONVERTERS, defaultConverter, ['test'], ObjectStrategy.FLAT, [{}]);

      expect(context.converters).toEqual(CONVERTERS);
      expect(context.defaultConverter).toEqual(defaultConverter);
      expect(context.groups).toEqual(['test']);
      expect(context.strategy).toEqual(ObjectStrategy.FLAT);
      expect(context.cyclic).toEqual([{}]);
    });

    it('with default cyclic', () => {
      const defaultConverter = new ObjectConverter();
      const context = new Context(CONVERTERS, defaultConverter, ['test'], ObjectStrategy.FLAT);

      expect(context.converters).toEqual(CONVERTERS);
      expect(context.defaultConverter).toEqual(defaultConverter);
      expect(context.groups).toEqual(['test']);
      expect(context.strategy).toEqual(ObjectStrategy.FLAT);
      expect(context.cyclic).toEqual([]);
    });

    it('with default strategy and cyclic', () => {
      const defaultConverter = new ObjectConverter();
      const context = new Context(CONVERTERS, defaultConverter, ['test']);

      expect(context.converters).toEqual(CONVERTERS);
      expect(context.defaultConverter).toEqual(defaultConverter);
      expect(context.groups).toEqual(['test']);
      expect(context.strategy).toEqual(ObjectStrategy.TREE);
      expect(context.cyclic).toEqual([]);
    });

    it('with default groups, decoratorOnly and cyclic', () => {
      const defaultConverter = new ObjectConverter();
      const context = new Context(CONVERTERS, defaultConverter);

      expect(context.converters).toEqual(CONVERTERS);
      expect(context.defaultConverter).toEqual(defaultConverter);
      expect(context.groups).toEqual([]);
      expect(context.strategy).toEqual(ObjectStrategy.TREE);
      expect(context.cyclic).toEqual([]);
    });
  });

  describe('can be instanced from parent', () => {

    it('from empty options', () => {
      const defaultConverter = new ObjectConverter();
      const parent = new Context(CONVERTERS, defaultConverter, ['test'], ObjectStrategy.FLAT, [{}]);
      const context = parent.childFromOption({});

      expect(parent === context).toBeFalsy();
      expect(context.converters).toEqual(CONVERTERS);
      expect(context.defaultConverter).toEqual(defaultConverter);
      expect(context.groups).toEqual(['test']);
      expect(context.strategy).toEqual(ObjectStrategy.FLAT);
      expect(context.cyclic).toEqual([{}]);
    });

    it('from options with groups', () => {
      const defaultConverter = new ObjectConverter();
      const parent = new Context(CONVERTERS, defaultConverter, ['test'], ObjectStrategy.FLAT, [{}]);
      const context = parent.childFromOption({groups: ['options'], strategy: ObjectStrategy.TREE});

      expect(parent === context).toBeFalsy();
      expect(context.converters).toEqual(CONVERTERS);
      expect(context.defaultConverter).toEqual(defaultConverter);
      expect(context.groups).toEqual(['options']);
      expect(context.strategy).toEqual(ObjectStrategy.TREE);
      expect(context.cyclic).toEqual([{}]);
    });

    it('from instance', () => {
      const defaultConverter = new ObjectConverter();
      const parent = new Context(CONVERTERS, defaultConverter, ['test'], ObjectStrategy.FLAT);
      const context = parent.childFromCyclic({});

      expect(parent === context).toBeFalsy();
      expect(context.converters).toEqual(CONVERTERS);
      expect(context.defaultConverter).toEqual(defaultConverter);
      expect(context.groups).toEqual(['test']);
      expect(context.strategy).toEqual(ObjectStrategy.FLAT);
      expect(context.cyclic).toEqual([{}]);
    });
  });

  describe('can test if groups is excluded', () => {

    it('with no groups', () => {
      const defaultConverter = new ObjectConverter();
      const context = new Context(CONVERTERS, defaultConverter, [], ObjectStrategy.TREE, []);

      expect(context.excludeFromGroups([])).toEqual(false);
      expect(context.excludeFromGroups(['test'])).toEqual(true);
    });

    it('with groups', () => {
      const defaultConverter = new ObjectConverter();
      const context = new Context(CONVERTERS, defaultConverter, ['test'], ObjectStrategy.TREE, []);

      expect(context.excludeFromGroups([])).toEqual(true);
      expect(context.excludeFromGroups(['test1'])).toEqual(true);
      expect(context.excludeFromGroups(['test'])).toEqual(false);
    });
  });

  describe('can test if cyclic is excluded', () => {

    it('with no cyclic', () => {
      const defaultConverter = new ObjectConverter();
      const context = new Context(CONVERTERS, defaultConverter, [], ObjectStrategy.TREE, []);

      expect(context.excludeFromCyclic(null)).toEqual(false);
    });

    it('with cyclic', () => {
      const defaultConverter = new ObjectConverter();
      const context = new Context(CONVERTERS, defaultConverter, ['test'], ObjectStrategy.TREE, [defaultConverter]);

      expect(context.excludeFromCyclic(null)).toEqual(false);
      expect(context.excludeFromCyclic(defaultConverter)).toEqual(true);
    });
  });

  describe('can find converter', () => {

    it('from type in array', () => {
      const defaultConverter = new ObjectConverter();
      const context = new Context(CONVERTERS, defaultConverter);

      expect(context.findConverterFromType(Array)).toEqual(CONVERTERS[7]);
    });

    it('from type in default', () => {
      const defaultConverter = new ObjectConverter();
      const context = new Context(CONVERTERS, defaultConverter);

      expect(context.findConverterFromType(ObjectConverter)).toEqual(defaultConverter);
    });

    it('from instance in array', () => {
      const defaultConverter = new ObjectConverter();
      const context = new Context(CONVERTERS, defaultConverter);

      expect(context.findConverterFromInstance([])).toEqual(CONVERTERS[7]);
    });

    it('from instance in default', () => {
      const defaultConverter = new ObjectConverter();
      const context = new Context(CONVERTERS, defaultConverter);

      expect(context.findConverterFromInstance(defaultConverter)).toEqual(defaultConverter);
    });
  });

  describe('can find type', () => {
    it('as void 0', () => {
      const context = new Context(CONVERTERS, CONVERTERS[7]);
      expect(context.findType(void 0)).toEqual(void 0);
    });

    it('as null', () => {
      const context = new Context(CONVERTERS, CONVERTERS[7]);
      expect(context.findType(null)).toEqual(null);
    });

    it('as boolean', () => {
      const context = new Context(CONVERTERS, CONVERTERS[7]);
      expect(context.findType(true)).toEqual(Boolean);
    });

    it('as number', () => {
      const context = new Context(CONVERTERS, CONVERTERS[7]);
      expect(context.findType(0)).toEqual(Number);
    });

    it('as string', () => {
      const context = new Context(CONVERTERS, CONVERTERS[7]);
      expect(context.findType('s')).toEqual(String);
    });

    it('as date', () => {
      const context = new Context(CONVERTERS, CONVERTERS[7]);
      expect(context.findType(new Date())).toEqual(Date);
    });

    it('as regexp', () => {
      const context = new Context(CONVERTERS, CONVERTERS[7]);
      expect(context.findType(/tt/)).toEqual(RegExp);
    });

    it('as array', () => {
      const context = new Context(CONVERTERS, CONVERTERS[7]);
      expect(context.findType([])).toEqual(Array);
    });

    it('as object', () => {
      const context = new Context(CONVERTERS, CONVERTERS[7]);
      expect(context.findType({})).toEqual(Object);
    });
  });
});
