import {NotAnObjectException} from '../../src';

describe('Exception ', () => {

  describe('"NotAnObjectException" should ', () => {

    it('throw an error if the value is not an object', () => {
      expect(() => NotAnObjectException.assert(null)).toThrowError('null is not an object');
    });

    it('return true if the value is an object', () => {
      expect(NotAnObjectException.assert({})).toBeTruthy();
    });
  });
});
