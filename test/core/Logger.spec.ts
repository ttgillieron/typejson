import {Level, Logger} from '../../src/core/Logger';

describe('Logger ', () => {

  describe('can be instanced from constructor', () => {

    it('with no default', () => {
      const logger = new Logger();

      expect(logger.getLevel()).toEqual(Level.WARN);
    });

    it('with default level', () => {
      const logger = new Logger(Level.TRACE);

      expect(logger.getLevel()).toEqual(Level.TRACE);
    });
  });

  describe('can be limit output in function of level', () => {

    let c: any;

    beforeEach(() => {
      c = {
        trace: (): any => { return null; },
        debug: (): any => { return null; },
        info: (): any => { return null; },
        warn: (): any => { return null; },
        error: (): any => { return null; }
      };

      spyOn(c, 'trace');
      spyOn(c, 'debug');
      spyOn(c, 'info');
      spyOn(c, 'warn');
      spyOn(c, 'error');
    });

    it('trace', () => {
      const logger = new Logger(Level.TRACE);
      logger.setConsole(c);

      logger.trace('e');
      logger.debug('e');
      logger.info('e');
      logger.warn('e');
      logger.error('e');
      expect(c.trace).toHaveBeenCalled();
      expect(c.debug).toHaveBeenCalled();
      expect(c.info).toHaveBeenCalled();
      expect(c.warn).toHaveBeenCalled();
      expect(c.error).toHaveBeenCalled();
    });

    it('debug', () => {
      const logger = new Logger(Level.DEBUG);
      logger.setConsole(c);

      logger.trace('e');
      logger.debug('e');
      logger.info('e');
      logger.warn('e');
      logger.error('e');
      expect(c.trace).not.toHaveBeenCalled();
      expect(c.debug).toHaveBeenCalled();
      expect(c.info).toHaveBeenCalled();
      expect(c.warn).toHaveBeenCalled();
      expect(c.error).toHaveBeenCalled();
    });

    it('info', () => {
      const logger = new Logger(Level.INFO);
      logger.setConsole(c);

      logger.trace('e');
      logger.debug('e');
      logger.info('e');
      logger.warn('e');
      logger.error('e');
      expect(c.trace).not.toHaveBeenCalled();
      expect(c.debug).not.toHaveBeenCalled();
      expect(c.info).toHaveBeenCalled();
      expect(c.warn).toHaveBeenCalled();
      expect(c.error).toHaveBeenCalled();
    });

    it('warn', () => {
      const logger = new Logger(Level.WARN);
      logger.setConsole(c);

      logger.trace('e');
      logger.debug('e');
      logger.info('e');
      logger.warn('e');
      logger.error('e');
      expect(c.trace).not.toHaveBeenCalled();
      expect(c.debug).not.toHaveBeenCalled();
      expect(c.info).not.toHaveBeenCalled();
      expect(c.warn).toHaveBeenCalled();
      expect(c.error).toHaveBeenCalled();
    });

    it('error', () => {
      const logger = new Logger(Level.ERROR);
      logger.setConsole(c);

      logger.trace('e');
      logger.debug('e');
      logger.info('e');
      logger.warn('e');
      logger.error('e');
      expect(c.trace).not.toHaveBeenCalled();
      expect(c.debug).not.toHaveBeenCalled();
      expect(c.info).not.toHaveBeenCalled();
      expect(c.warn).not.toHaveBeenCalled();
      expect(c.error).toHaveBeenCalled();
    });

    it('silent', () => {
      const logger = new Logger(Level.SILENT);
      logger.setConsole(c);

      logger.trace('e');
      logger.debug('e');
      logger.info('e');
      logger.warn('e');
      logger.error('e');
      expect(c.trace).not.toHaveBeenCalled();
      expect(c.debug).not.toHaveBeenCalled();
      expect(c.info).not.toHaveBeenCalled();
      expect(c.warn).not.toHaveBeenCalled();
      expect(c.error).not.toHaveBeenCalled();
    });
  });

  describe('do not error if method is not available', () => {

    it('call to log', () => {
      const c = {log: (): any => { return null; }};
      spyOn(c, 'log');

      const logger = new Logger(Level.TRACE);
      logger.setConsole(c);

      logger.error('e');
      expect(c.log).toHaveBeenCalled();
    });

    it('call to noop', () => {
      const c = {noop: (): any => { return null; }};
      spyOn(c, 'noop');

      const logger = new Logger(Level.TRACE);
      logger.setConsole(c);

      logger.error('e');
      expect(c.noop).not.toHaveBeenCalled();
    });
  });
});
