import {getPropertyDefinition, KEY_EXPOSE, PropertyDefinition, setDefinition} from '../../src/definitions/PropertyDefinition';
import {nofct, ObjectStrategy} from '../../src/core/Context';

export class TestDefinition {
  property: string;
}

function Dumy(target: any, property?: string) {
}

export class TestDefinitionWithDecorator {
  @Dumy property: string;
}


describe('PropertyDefinition ', () => {

  it('can be instanced from constructor', () => {
    const toPlain = (): any => null;
    const fromPlain = (): any => null;

    const def = new PropertyDefinition(
      'property',
      'plain',
      String,
      Array,
      'value',
      ['groups'],
      true,
      true,
      toPlain,
      fromPlain,
      ObjectStrategy.FLAT
    );

    expect(def.property).toEqual('property');
    expect(def.plain).toEqual('plain');
    expect(def.type).toEqual(String);
    expect(def.generic).toEqual(Array);
    expect(def.value).toEqual('value');
    expect(def.groups).toEqual(['groups']);
    expect(def.excludeToPlain).toEqual(true);
    expect(def.excludeFromPlain).toEqual(true);
    expect(def.toPlain).toEqual(toPlain);
    expect(def.fromPlain).toEqual(fromPlain);
    expect(def.strategy).toEqual(ObjectStrategy.FLAT);
  });

  describe('can get definition from instance ', () => {
    it('when it exist', () => {
      const setDef = new PropertyDefinition(
        'property',
        'plain',
        String,
        Array,
        'value',
        ['groups'],
        true,
        true,
        nofct,
        nofct,
        ObjectStrategy.FLAT
      );
      const instance = new TestDefinition();
      Reflect.defineMetadata(KEY_EXPOSE, [setDef], instance, 'property');
      const def = getPropertyDefinition(instance, 'property')[0];

      expect(def).toEqual(setDef);
    });

    it('when it does not exist without decorator', () => {
      const instance = new TestDefinitionWithDecorator();
      const def = getPropertyDefinition(instance, 'property')[0];

      expect(def.property).toEqual('property');
      expect(def.plain).toEqual('property');
      expect(def.type).toEqual(void 0);
      expect(def.generic).toEqual(void 0);
      expect(def.value).toEqual(void 0);
      expect(def.groups).toEqual([]);
      expect(def.excludeToPlain).toEqual(true);
      expect(def.excludeFromPlain).toEqual(true);
      expect(def.toPlain).toEqual(nofct);
      expect(def.fromPlain).toEqual(nofct);
      expect(def.strategy).toEqual(ObjectStrategy.TREE);
    });

  });

  describe('can set definition on instance ', () => {
    it('when no definition exist', () => {
      const def = new PropertyDefinition(
        'property',
        'plain',
        String,
        Array,
        'value',
        ['groups'],
        true,
        true,
        nofct,
        nofct,
        ObjectStrategy.FLAT
      );
      const instance = new TestDefinition();
      setDefinition(instance, 'property', def);

      expect(Reflect.getMetadata(KEY_EXPOSE, instance, 'property')).toEqual([def]);
    });

    it('when a definition exist with groups', () => {
      const def = new PropertyDefinition(
        'property',
        'plain',
        String,
        Array,
        'value',
        ['groups'],
        true,
        true,
        nofct,
        nofct,
        ObjectStrategy.FLAT
      );
      const instance = new TestDefinition();
      setDefinition(instance, 'property', def);
      setDefinition(instance, 'property', def);

      expect(Reflect.getMetadata(KEY_EXPOSE, instance, 'property')).toEqual([def, def]);
    });

    it('when a definition exist without groups', () => {
      const def = new PropertyDefinition(
        'property',
        'plain',
        String,
        Array,
        'value',
        [],
        true,
        true,
        nofct,
        nofct,
        ObjectStrategy.FLAT
      );
      const instance = new TestDefinition();
      setDefinition(instance, 'property', def);
      setDefinition(instance, 'property', def);

      expect(Reflect.getMetadata(KEY_EXPOSE, instance, 'property')).toEqual([def, def]);
    });
  });
});
