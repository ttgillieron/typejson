import {Converter} from '../core/Converter';
import {Context, nofct, ObjectStrategy} from '../core/Context';
import {getPropertyDefinition, PropertyDefinition} from '../definitions/PropertyDefinition';
import {LOGGER} from '../core/Logger';

export class ObjectConverter extends Converter {
  match(type: any): boolean { return type === Object; }

  toPlainObject(instance: any, context: Context) {
    LOGGER.debug('toPlainObject start', instance, context);

    const plain: any = {};
    Object.keys(instance)
          .map(key => getPropertyDefinition(instance, key))
          .reduce((res: PropertyDefinition[], defs: PropertyDefinition[]) => res.concat(defs), [])
          .filter(def => !(def.excludeToPlain || context.excludeFromGroups(def.groups) || context.excludeFromCyclic(instance[def.property])))
          .forEach(def => this.playStrategyToJson(def, context, plain, this.playToJsonConverterFunction(def, instance, plain, context)));
    this.playHook('onToPlainObject', instance, plain, context);

    LOGGER.debug('toPlainObject end', plain);
    return plain;
  }

  fromPlainObject(type: any, plain: any, context: Context) {
    LOGGER.debug('fromPlainObject start', plain, context);

    const instance = new type();
    Object.keys(instance)
          .map(key => getPropertyDefinition(instance, key))
          .reduce((res: PropertyDefinition[], defs: PropertyDefinition[]) => res.concat(defs), [])
          .filter(def => !(def.excludeFromPlain || context.excludeFromGroups(def.groups)))
          .forEach(def => instance[def.property] = this.playFromJsonConverterFunction(def, instance, plain, context));
    this.playHook('onFromPlainObject', instance, plain, context);

    LOGGER.debug('fromPlainObject end', instance);
    return instance;
  }

  private playToJsonConverterFunction(def: PropertyDefinition, instance: any, plain: any, context: Context): any {
    const value = def.toPlain !== nofct ? def.toPlain(instance[def.property]) :
      context.findConverterFromInstance(instance[def.property]).toPlainObject(instance[def.property], context.childFromCyclic(instance));
    return value == null ? def.value : value;
  }

  private playFromJsonConverterFunction(def: PropertyDefinition, instance: any, plain: any, context: Context): any {
    const converter = context.findConverterFromType(def.generic);

    if (this.deduceStrategy(def, context) === ObjectStrategy.FLAT && converter instanceof ObjectConverter) {
      return def.fromPlain !== nofct ? def.fromPlain(plain) : converter.fromPlainObject(def.type, plain, context);
    }

    return def.fromPlain !== nofct ? def.fromPlain(plain[def.plain]) : converter.fromPlainObject(def.type, plain[def.plain], context);
  }

  private  playStrategyToJson(def: PropertyDefinition, context: Context, plain: any, value: any): any {
    if (this.deduceStrategy(def, context) === ObjectStrategy.FLAT && context.findConverterFromInstance(value) instanceof ObjectConverter) {
      Object.keys(value).forEach(key => plain[key] = value[key]);
    } else {
      plain[def.plain] = value;
    }
  }

  private playHook(hook: string, instance: any, plain: any, context: Context) {
    if (instance[hook] == null) { return; }
    instance[hook](plain, context);
  }

  private deduceStrategy(def: PropertyDefinition, context: Context): ObjectStrategy {
    return def.strategy !== ObjectStrategy.NONE ? def.strategy : context.strategy !== ObjectStrategy.NONE ? context.strategy : ObjectStrategy.TREE;
  }
}
