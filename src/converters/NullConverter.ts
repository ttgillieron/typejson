import {Converter} from '../core/Converter';

export class NullConverter extends Converter {
  match(type: any): boolean { return type === null; }

  toPlainObject(): any { return null; }

  fromPlainObject(): any { return null; }

}
