import {Converter} from '../core/Converter';

export class DateConverter extends Converter {

  match(type: any): boolean { return type === Date; }

  toPlainObject(instance: any) { return instance.toISOString(); }

  fromPlainObject(type: any, plain: any) { return new Date(plain); }
}
