import {UndefinedConverter} from './UndefinedConverter';
import {NullConverter} from './NullConverter';
import {StringConverter} from './StringConverter';
import {NumberConverter} from './NumberConverter';
import {BooleanConverter} from './BooleanConverter';
import {DateConverter} from './DateConverter';
import {RegexpConverter} from './RegexpConverter';
import {ArrayConverter} from './ArrayConverter';
import {ObjectConverter} from './ObjectConverter';

export const CONVERTERS = [
  new UndefinedConverter(),
  new NullConverter(),
  new StringConverter(),
  new NumberConverter(),
  new BooleanConverter(),
  new DateConverter(),
  new RegexpConverter(),
  new ArrayConverter(),
  new ObjectConverter()
];
