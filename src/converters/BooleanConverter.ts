import {Converter} from '../core/Converter';

export class BooleanConverter extends Converter {

  match(type: any): boolean { return type === Boolean; }

  toPlainObject(instance: any) { return instance; }

  fromPlainObject(type: any, plain: any) { return plain; }
}
