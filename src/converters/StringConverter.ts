import {Converter} from '../core/Converter';

export class StringConverter extends Converter {

  match(type: any): boolean { return type === String; }

  toPlainObject(instance: any) { return instance; }

  fromPlainObject(type: any, plain: any) { return plain; }
}
