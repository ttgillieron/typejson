import {Converter} from '../core/Converter';

export class UndefinedConverter extends Converter {

  match(type: any): boolean { return type === void 0; }

  toPlainObject(): any { return void 0; }

  fromPlainObject(): any { return void 0; }
}
