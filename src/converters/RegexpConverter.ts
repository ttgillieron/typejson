import {Converter} from '../core/Converter';

export class RegexpConverter extends Converter {

  match(type: any): boolean { return type === RegExp; }

  toPlainObject(instance: any) { return instance.toString(); }

  fromPlainObject(type: any, plain: any) { return new RegExp(plain); }
}
