import {Converter} from '../core/Converter';
import {Context} from '../core/Context';

export class ArrayConverter extends Converter {

  match(type: any): boolean { return type === Array; }

  toPlainObject(instance: any[], context: Context) {
    return instance.map(i => context.findConverterFromInstance(i).toPlainObject(i, context));
  }

  fromPlainObject(type: any, plain: any[], context: Context) {
    return plain.map(p => context.findConverterFromType(type).toPlainObject(p, context));
  }
}
