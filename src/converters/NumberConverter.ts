import {Converter} from '../core/Converter';

export class NumberConverter extends Converter {

  match(type: any): boolean { return type === Number; }

  toPlainObject(instance: any) { return instance; }

  fromPlainObject(type: any, plain: any) { return plain; }
}
