import {nofct, ObjectStrategy, Type} from '../core/Context';
import {KEY_REFLECT_TYPE, PropertyDefinition, setDefinition} from '../definitions/PropertyDefinition';

export interface ExposeMetadata {
  plain?: string;
  type?: Type;
  value?: any;
  groups?: string[];
  excludeToPlain?: boolean;
  excludeFromPlain?: boolean;
  toPlain?: (value: any) => any;
  fromPlain?: (value: any) => any;
  strategy?: ObjectStrategy;
}

export function Expose(metadata: ExposeMetadata = {}) {
  return (target: any, propertyKey: string) =>
    setDefinition(target, propertyKey, definitionFromMetadata(propertyKey, Reflect.getMetadata(KEY_REFLECT_TYPE, target, propertyKey), metadata));
}

function definitionFromMetadata(property: string, generic: Type, metadata: ExposeMetadata): PropertyDefinition {
  return new PropertyDefinition(
    property,
    metadata.plain != null ? metadata.plain : property,
    metadata.type != null ? metadata.type : generic,
    generic,
    metadata.value != null ? metadata.value : null,
    Array.isArray(metadata.groups) ? metadata.groups : [],
    metadata.excludeToPlain != null ? metadata.excludeToPlain : false,
    metadata.excludeFromPlain != null ? metadata.excludeFromPlain : false,
    metadata.toPlain != null ? metadata.toPlain : nofct,
    metadata.fromPlain != null ? metadata.fromPlain : nofct,
    metadata.strategy != null ? metadata.strategy : ObjectStrategy.NONE,
  );
}
