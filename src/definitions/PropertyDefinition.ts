import {nofct, ObjectStrategy, Type} from '../core/Context';
import {LOGGER} from '../core/Logger';

export const KEY_REFLECT_TYPE = 'design:type';
export const KEY_EXPOSE = 'types:expose:property';

export class PropertyDefinition {
  /**
   * property name
   */
  property: string;

  /**
   * property name on plain object
   */
  plain: string;

  /**
   * property type eg Number
   */
  type: Type;

  /**
   * generic type eg Array. If none is provided if will be equals to `type`
   */
  generic: Type;

  /**
   * default value
   */
  value: any;

  /**
   * groups the definition apply to
   */
  groups: string[];

  /**
   * exclude from instance to plain
   */
  excludeToPlain: boolean;

  /**
   * exclude from plain to instance
   */
  excludeFromPlain: boolean;

  toPlain: (value: any) => any;

  fromPlain: (value: any) => any;

  strategy: ObjectStrategy;

  constructor(property: string,
              plain: string,
              type: Type,
              generic: Type,
              value: any,
              groups: string[],
              excludeToPlain: boolean,
              excludeFromPlain: boolean,
              toPlain: (value: any) => any,
              fromPlain: (value: any) => any,
              strategy: ObjectStrategy) {
    this.property = property;
    this.plain = plain;
    this.type = type;
    this.generic = generic;
    this.value = value;
    this.groups = groups;
    this.excludeToPlain = excludeToPlain;
    this.excludeFromPlain = excludeFromPlain;
    this.toPlain = toPlain;
    this.fromPlain = fromPlain;
    this.strategy = strategy;
  }
}

export function getPropertyDefinition(instance: any, property: string): PropertyDefinition[] {
  return Reflect.hasMetadata(KEY_EXPOSE, instance, property) ? Reflect.getMetadata(KEY_EXPOSE, instance, property) :
    [new PropertyDefinition(property, property, void 0, void 0, void 0, [], true, true, nofct, nofct, ObjectStrategy.TREE)];
}

export function setDefinition(instance: any, property: string, definition: PropertyDefinition) {
  const definitions: PropertyDefinition[] = Reflect.hasMetadata(KEY_EXPOSE, instance, property) ?
    Reflect.getMetadata(KEY_EXPOSE, instance, property) : [];

  if (definitions.filter(def => {
     return (def.groups.length === 0 && definition.groups.length === 0 ) ? true :
        def.groups.filter(g1 => definition.groups.filter(g2 => g2 === g1).length > 0).length > 0;
    }).length > 0) {
    LOGGER.warn(`definition for group ${definition.groups} already exist`, definition);
  }

  Reflect.defineMetadata(KEY_EXPOSE, definitions.concat(definition), instance, property);
}
