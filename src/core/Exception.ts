export class NotAnObjectException {
  static assert(value: any): boolean {
    if (value instanceof Object) { return true; }
    throw new Error(value + ' is not an object');
  }
}
