import {NotAnObjectException} from './Exception';
import {Context, Options, Type} from './Context';
import {ObjectConverter} from '../converters/ObjectConverter';
import {CONVERTERS} from '../converters/index';

export class Serializer {
  private context: Context;

  constructor(context?: Context) {
    this.context = context || new Context(CONVERTERS, new ObjectConverter());
  }

  toJson = (instance: any, options?: Options): string => JSON.stringify(this.toPlainObject(instance, options));
  fromJson = (type: Type, json: string, options?: Options): any => this.fromPlainObject(type, JSON.parse(json), options);

  toPlainObject(instance: any, options: Options = {}): any {
    NotAnObjectException.assert(instance);
    const context = this.context.childFromOption(options);
    return context.findConverterFromInstance(instance).toPlainObject(instance, context);
  }

  fromPlainObject(type: Type, plain: any, options: Options = {}): any {
    NotAnObjectException.assert(type);
    NotAnObjectException.assert(plain);
    const context = this.context.childFromOption(options);
    return context.findConverterFromType(Array.isArray(plain) ? Array : type).fromPlainObject(type, plain, context);
  }
}

// TODO inheritance
// TODO schema

// TODO complex test
// TODO comments
// TODO try-catch/logs
// TODO test annotation on get/set + fct
