import {Context, Type} from './Context';

export abstract class Converter {

  abstract match(type: Type): boolean;

  abstract toPlainObject(instance: any, context: Context): any;

  abstract fromPlainObject(type: Type, plain: any, context: Context): any;
}
