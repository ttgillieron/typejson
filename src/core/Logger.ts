export enum Level { TRACE, DEBUG, INFO, WARN, ERROR, SILENT }

const names = ['trace', 'debug', 'info', 'warn', 'error'];

export class Logger {
  private console = console;

  constructor(private level: Level = Level.WARN) {}

  setConsole = (c: any) => this.console = c;
  setLevel = (level: Level) => this.level = level;
  getLevel = (): Level => this.level;

  trace = (...args: any[]): void => this.write(Level.TRACE, args);
  debug = (...args: any[]): void => this.write(Level.DEBUG, args);
  info = (...args: any[]): void => this.write(Level.INFO, args);
  warn = (...args: any[]): void => this.write(Level.WARN, args);
  error = (...args: any[]): void => this.write(Level.ERROR, args);

  private write = (level: Level, args: any[]) => level < this.level ? null : this.getConsole(level)(names[level].toUpperCase(), ...args);
  private getConsole = (level: Level): any => {
    return (<any>this.console)[names[level]] != null ? (<any>this.console)[names[level]] : this.console.log != null ? this.console.log : (): any => null;
  }
}

export const LOGGER = new Logger();
