import {Converter} from './Converter';

export enum ObjectStrategy { TREE, FLAT, NONE }

export interface Options {
  groups?: string[];
  schemaId?: string;
  strategy?: ObjectStrategy;
}

export type Type = any;
export const nofct = (): any => null;

export class Context {

  converters: Converter[];
  defaultConverter: Converter;

  cyclic: any[];

  groups: string[];
  strategy: ObjectStrategy;

  constructor(converters: Converter[],
              defaultConverter: Converter,
              groups: string[] = [],
              strategy: ObjectStrategy = ObjectStrategy.TREE,
              cyclic: any[] = []) {
    this.converters = converters;
    this.defaultConverter = defaultConverter;
    this.groups = groups;
    this.strategy = strategy;
    this.cyclic = cyclic;
  }

  findConverterFromType(type: Type): Converter {
    const find = this.converters.filter(c => c.match(type));
    return find.length > 0 ? find[0] : this.defaultConverter;
  }

  findConverterFromInstance(instance: any): Converter {
    return this.findConverterFromType(this.findType(instance));
  }

  excludeFromGroups(groups: string[]): boolean {
    return (this.groups.length === 0 && groups.length === 0 ) ? false :
      this.groups.filter(g1 => groups.filter(g2 => g2 === g1).length > 0).length === 0;
  }

  excludeFromCyclic(instance: any): boolean {
    return this.cyclic.filter(c => c === instance).length > 0;
  }

  childFromOption(option: Options): Context {
    return new Context(
      this.converters,
      this.defaultConverter,
      option.groups != null ? option.groups : this.groups,
      option.strategy != null ? option.strategy : this.strategy,
      this.cyclic
    );
  }

  childFromCyclic(instance: any): Context {
    return new Context(
      this.converters,
      this.defaultConverter,
      this.groups,
      this.strategy,
      this.cyclic.concat(instance)
    );
  }

  findType(instance: any): any {
    if (instance === null) { return null; }
    if (typeof instance === 'boolean') { return Boolean; }
    if (typeof instance === 'number') { return Number; }
    if (typeof instance === 'string') { return String; }
    if (instance instanceof Object) { return instance.constructor; }
    return void 0;
  }
}
